<?php
/**
 * Copyright 2015 Carlo Nicora
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @license Apache
 * @license http://www.apache.org/licenses/LICENSE-2.0
 * @package CarloNicora\cryogen\redisCryogen
 * @author Carlo Nicora
 */

namespace CarloNicora\cryogen\redisCryogen;

use CarloNicora\cryogen\connectionController;
use Redis;

/**
 * Implementation of the connection controller for redis
 *
 * @package CarloNicora\cryogen\redisCryogen
 */
class redisConnectionController extends connectionController{
    /**
     * @var redisConnectionBuilder $connectionValues
     */
    public $connectionValues;

    /**
     * Opens a connection to the database
     *
     * @return bool
     */
    public function connect(){
        $returnValue = true;

        if (!isset($this->connection)) {
            $this->connection = new Redis();
            $this->connection->connect($this->connectionValues->host, $this->connectionValues->port);
            $this->connection->select($this->connectionValues->databaseNumber);
        }

        return($returnValue);
    }

    /**
     * Closes a connection to the database
     *
     * @return bool
     */
    public function disconnect(){
        if($this->isConnected()){
            $this->connection->close();
        }
    }

    /**
     * Returns the name of the database specified in the connection
     *
     * @return string
     */
    public function getDatabaseName(){
        return(null);
    }

    /**
     * Create a new Database
     *
     * @param string $databaseName
     * @return bool
     */
    public function createDatabase($databaseName){
        $returnValue = false;

        return($returnValue);
    }

    /**
     * Identifies if there is an active connection to the database
     *
     * @return bool
     */
    public function isConnected(){
        return(isset($this->connection));
    }
}
?>