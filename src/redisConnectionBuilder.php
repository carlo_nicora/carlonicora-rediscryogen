<?php
/**
 * Copyright 2015 Carlo Nicora
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @license Apache
 * @license http://www.apache.org/licenses/LICENSE-2.0
 * @package CarloNicora\cryogen
 * @author Carlo Nicora
 */
namespace CarloNicora\cryogen\redisCryogen;

use CarloNicora\cryogen\connectionBuilder;

/**
 * Class redisConnectionBuilder
 *
 * @package CarloNicora\cryogen\redisCryogen
 */
class redisConnectionBuilder extends connectionBuilder{
    /** @var string */
    public $host;

    /** @var int */
    public $port;

    /** @var int */
    public $databaseNumber;

    /** @var string */
    public $databaseName;

    /**
     * Initialises the connection parameters in the database-type-specific connection builder
     *
     * @param array $connectionValues
     */
    public function initialise($connectionValues){
        $this->databaseType = 'redis';

        $this->host = $connectionValues['host'];
        $this->port = $connectionValues['port'];
        $this->databaseNumber = $connectionValues['databasenumber'];
    }

    /**
     * Extends the database name of the connection builder
     *
     * @param string $databaseName
     */
    public function extendDatabaseName($databaseName){
    }
}