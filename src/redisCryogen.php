<?php
/**
 * Copyright 2015 Carlo Nicora
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @license Apache
 * @license http://www.apache.org/licenses/LICENSE-2.0
 * @package CarloNicora\cryogen\redisCryogen
 * @author Carlo Nicora
 */
namespace CarloNicora\cryogen\redisCryogen;

use CarloNicora\cryogen\cryogen;
use CarloNicora\cryogen\entity;
use CarloNicora\cryogen\entityList;
use CarloNicora\cryogen\metaField;
use CarloNicora\cryogen\queryEngine;
use CarloNicora\cryogen\metaTable;

/**
 * Main class for the redis plugin for cryogen
 *
 * @package CarloNicora\cryogen\redisCryogen
 */
class redisCryogen extends cryogen{
    /**
     * Initialises cryogen for redis
     *
     * @param redisConnectionBuilder $connection
     */
    public function __construct($connection){
        $returnValue = false;

        $this->connectionController = new redisConnectionController();

        if ($this->connectionController->initialize($connection)){
            $this->structureController = new redisStructureController($this->connectionController, $this);
            $returnValue = true;
        }

        return($returnValue);
    }

    /**
     * Returns if a database exists
     *
     * @param string $databaseName
     * @return bool
     */
    public function databaseExists($databaseName){
        return(false);
    }

    /**
     * @param metaTable|null $meta
     * @param entity|null $entity
     * @param null $valueOfKeyField
     * @return mixed
     */
    public function generateQueryEngine(metaTable $meta=null, entity $entity=null, $valueOfKeyField=null){
        $returnValue = new redisQueryEngine($meta, $entity, $valueOfKeyField);

        return($returnValue);
    }

    /**
     * Clears the resources
     */
    public function __destruct(){
        if (isset($this->connectionController) && $this->connectionController->isConnected()){
            $this->connectionController->disconnect();
        } else if (isset($this->connectionController)){
            unset($this->connectionController);
        }
    }

    /**
     * Commit the INSERT, UPDATE or DELETE transaction on the database
     *
     * @param bool $commit
     * @return bool
     */
    protected function completeActionTransaction($commit){
        return (false);
    }

    /**
     * Returns the number of records matching the query in the query engine
     *
     * @param queryEngine $engine
     * @return int
     */
    public function count(queryEngine $engine) {
        return(0);
    }

    /**
     * Deletes an entity in the database.
     *
     * @param entity|entityList|null $entity
     * @param queryEngine|null $engine
     * @return bool
     */
    public function delete($entity = null, queryEngine $engine = null) {
        if (isset($entity)){
            /*
             * NOT YET SUPPORTED
             */
            $returnValue = false;
        } else {
            $returnValue = $this->connectionController->connection->delete($engine->key);
        }
        return($returnValue);
    }

    /**
     * Reads a list of records identified by the query engine.
     *
     * If the levels of relations to load is > 0, then cryogen will load records related to a single foreign key as
     * defined in the database objects
     *
     * @param queryEngine $engine
     * @param int $levelsOfRelationsToLoad
     * @param metaTable|null $metaTableCaller
     * @param metaField|null $metaFieldCaller
     * @param bool $isSingle
     * @return entity|entityList|null
     */
    public function read(queryEngine $engine, $levelsOfRelationsToLoad = 0, metaTable $metaTableCaller = null, metaField $metaFieldCaller = null, $isSingle = false){
        $values = json_decode($this->connectionController->connection->get($engine->key));

        if (isset($values)) {
            if (is_array($values)) {
                $returnValue = new entityList($engine->meta);
                foreach ($values as $singleValue) {
                    $returnValue[] = $singleValue;
                }
            } else {
                $returnValue = $values;
            }
        }else {
            $returnValue = null;
        }

        return($returnValue);
    }

    /**
     * Updates an entity in the database.
     *
     * If the entity is not existing in the database, cryogen performs an INSERT, otherwise an UPDATE
     *
     * @param entity|entityList $entity
     * @param string $key
     * @return bool
     */
    public function update($entity, $key=null){
        if ($entity->isEntityList){
            $value = [];
            foreach ($entity as $singleEntity){
                $value[] = $singleEntity;
            }

            $value = json_encode($value);
        } else {
            $value = json_encode($entity);
        }

        $returnValue = $this->connectionController->connection->set($key, $value);

        return($returnValue);
    }

    /**
     * Reads one single record identified by the query engine.
     *
     * If the query returns more than one record, the system generates an error. This function is designed to return
     * a single-record query, not the first of many records.
     * If the levels of relations to load is > 0, then cryogen will load records related to a single foreign key as
     * defined in the database objects
     *
     * @param queryEngine $engine
     * @param int $levelsOfRelationsToLoad
     * @param metaTable|null $metaTableCaller
     * @param metaField|null $metaFieldCaller
     * @return entity|null
     */
    public function readSingle(queryEngine $engine, $levelsOfRelationsToLoad = 0, metaTable $metaTableCaller = null, metaField $metaFieldCaller = null) {
        return(null);
    }

    /**
     * Runs the transactional INSERT, UPDATE or DELETE query on the database
     *
     * @param string $sqlStatement
     * @param array $sqlParameters
     * @param bool $isDelete
     * @param bool $generatedId
     * @return entityList
     */
    protected function setActionTransaction($sqlStatement, $sqlParameters, $isDelete = false, &$generatedId = false) {
        return(null);
    }

    /**
     * Specialised transaction that counts the records matching a specific query engine on the database
     *
     * @param queryEngine $engine
     * @param string $sqlStatement
     * @param array $sqlParameters
     * @return int
     */
    protected function setCountTransaction(queryEngine $engine, $sqlStatement, $sqlParameters) {
        return(0);
    }

    /**
     * Runs the transactional SELECT query on the database
     *
     * @param queryEngine $engine
     * @param string $sqlStatement
     * @param array $sqlParameters
     * @return entityList
     */
    protected function setReadTransaction(queryEngine $engine, $sqlStatement, $sqlParameters){
        return(null);
    }
}
?>